export default /* GraphQL */ `
  mutation {
    host1: CreateHost(id: "host1", hostname: "host1", ipAddresses:["192.168.0.1", "10.0.0.1"]) {
      id
      hostname
    }
    h1SockAddr1: CreateSockAddress(id: "192.168.0.1:8080", ipAddress: "192.168.0.1", portNumber: 8080) {
      id
      ipAddress
      portNumber
    }
    h1SockAddr2: CreateSockAddress(id: "192.168.0.1:8090", ipAddress: "192.168.0.1", portNumber: 8090) {
      id
      ipAddress
      portNumber
    }
    h1SockAddr3: CreateSockAddress(id: "192.168.0.1:7001", ipAddress: "192.168.0.1", portNumber: 7001) {
      id
      ipAddress
      portNumber
    }
    h1SockAddr4: CreateSockAddress(id: "192.168.0.1:7002", ipAddress: "192.168.0.1", portNumber: 7002) {
      id
      ipAddress
      portNumber
    }
    h1SockAddr5: CreateSockAddress(id: "10.0.0.1:443", ipAddress: "10.0.0.1", portNumber: 443) {
      id
      ipAddress
      portNumber
    }
    h1SockAddr6: CreateSockAddress(id: "10.0.0.1:9001", ipAddress: "10.0.0.1", portNumber: 9001) {
      id
      ipAddress
      portNumber
    }


    h1sa1: AddHostSockAddresses(from: {id: "host1"}, to: {id: "192.168.0.1:8080"}) {
      from {
        id
      }
    }
    h1sa2: AddHostSockAddresses(from: {id: "host1"}, to: {id: "192.168.0.1:8090"}) {
      from {
        id
      }
    }
    h1sa3: AddHostSockAddresses(from: {id: "host1"}, to: {id: "192.168.0.1:7001"}) {
      from {
        id
      }
    }
    h1sa4: AddHostSockAddresses(from: {id: "host1"}, to: {id: "192.168.0.1:7002"}) {
      from {
        id
      }
    }
    h1sa5: AddHostSockAddresses(from: {id: "host1"}, to: {id: "10.0.0.1:443"}) {
      from {
        id
      }
    }
    h1sa6: AddHostSockAddresses(from: {id: "host1"}, to: {id: "10.0.0.1:9001"}) {
      from {
        id
      }
    }


    h1np1: CreateNativeProcess (id: "h1np1", processId: 3245, procCommandLine: "/usr/local/bin/httpd --salvia=divinorum", procImagePath: "/usr/local/bin/httpd", procEnvironment: []) {
      id
      processId
    }
    h1np2: CreateNativeProcess (id: "h1np2", processId: 3246, procCommandLine: "/usr/bin/httpd --salvia=divinorum" , procImagePath: "/usr/bin/httpd", procEnvironment: ["CC=/usr/bin/gcc"]) {
      id
      processId
    }
    h1np3: CreateNativeProcess (id: "h1np3", processId: 5693, procCommandLine: "/opt/app/oracle/wlserver_12.1/common/bin/java -jar wide_mouth.jar -argMush" , procImagePath: "/opt/app/oracle/wlserver_12.1/common/bin/java", procEnvironment: ["CLASSPATH=/opt/app/oracle/wlserver_12.1/lib/all_shaded.jar"]) {
      id
      processId
    }
    h1np4: CreateNativeProcess (id: "h1np4", processId: 5695, procCommandLine: "/opt/oracle/wlserver_12.1/common/bin/java -jar wide_mouth.jar -argMush" , procImagePath: "/opt/oracle/wlserver_12.1/common/bin/java", procEnvironment: ["CLASSPATH=/opt/oracle/wlserver_12.1/lib/all_shaded.jar"]) {
      id
      processId
    }
    h1np5: CreateNativeProcess (id: "h1np5", processId: 2849, procCommandLine: "/usr/share/hap/bin/haproxy", procImagePath: "/usr/share/hap/bin/haproxy", procEnvironment: []) {
      id
      processId
    }


    h1np1lo1: AddNativeProcessListensOn(from: {id: "h1np1"}, to: {id: "192.168.0.1:8080"}) {
      from {
        id
      }
    }
    h1np2lo1: AddNativeProcessListensOn(from: {id: "h1np2"}, to: {id: "192.168.0.1:8090"}) {
      from {
        id
      }
    }
    h1np3lo1: AddNativeProcessListensOn(from: {id: "h1np3"}, to: {id: "192.168.0.1:7001"}) {
      from {
        id
      }
    }
    h1np4lo1: AddNativeProcessListensOn(from: {id: "h1np4"}, to: {id: "192.168.0.1:7002"}) {
      from {
        id
      }
    }
    h1np5lo1: AddNativeProcessListensOn(from: {id: "h1np5"}, to: {id: "10.0.0.1:443"}) {
      from {
        id
      }
    }
    h1np5lo2: AddNativeProcessListensOn(from: {id: "h1np5"}, to: {id: "10.0.0.1:9001"}) {
      from {
        id
      }
    }

    # 4 external client connections accepted by HAProxy from 10.0.50.10
    h1np5accepted1: CreateConnections(id: "h1np5accepted1", ipAddresses: ["10.0.50.10"], count: [4]) {
      id
    }
    # 3 external client connections accepted by HAProxy from 10.0.50.11
    h1np5accepted2: CreateConnections(id: "h1np5accepted2", ipAddresses: ["10.0.50.11"], count: [3]) {
      id
    }
    # 4 connections connected out from HA proxy to httpd1
    h1np5connected1: CreateConnections(id: "h1np5connected1", ipAddresses: ["192.168.0.1"], count: [1]) {
      id
    }
    h1np5connected2: CreateConnections(id: "h1np5connected2", ipAddresses: ["192.168.0.1"], count: [1]) {
      id
    }
    h1np5connected3: CreateConnections(id: "h1np5connected3", ipAddresses: ["192.168.0.1"], count: [1]) {
      id
    }
    h1np5connected4: CreateConnections(id: "h1np5connected4", ipAddresses: ["192.168.0.1"], count: [1]) {
      id
    }
    # 3 connections connected out from HA proxy to httpd2
    h1np5connected5: CreateConnections(id: "h1np5connected5", ipAddresses: ["192.168.0.1"], count: [1]) {
      id
    }
    h1np5connected6: CreateConnections(id: "h1np5connected6", ipAddresses: ["192.168.0.1"], count: [1]) {
      id
    }
    h1np5connected7: CreateConnections(id: "h1np5connected7", ipAddresses: ["192.168.0.1"], count: [1]) {
      id
    }
    # 4 connections accepted by httpd1 from HA proxy
    h1np1accepted: CreateConnections(id: "h1np1accepted", ipAddresses: ["10.0.0.1"], count: [4]) {
      id
    }
    # 3 connections accepted by httpd2 from HA proxy
    h1np2accepted: CreateConnections(id: "h1np2accepted", ipAddresses: ["10.0.0.1"], count: [3]) {
      id
    }
    # 1 connections connected by httpd1 to HA proxy weblogic port, for forward to wl1
    h1np1connected1: CreateConnections(id: "h1np1connected1", ipAddresses: ["10.0.0.1"], count: [1]) {
      id
    }
    # 2 connections connected to HA proxy weblogic port from httpd2, 1 for forwarding to wl1 and another for forwarding to wl2
    h1np2connected1: CreateConnections(id: "h1np2connected1", ipAddresses: ["10.0.0.1"], count: [1]) {
      id
    }
    h1np2connected2: CreateConnections(id: "h1np2connected2", ipAddresses: ["10.0.0.1"], count: [1]) {
      id
    }

    # 3 internal client connections accepted by HAProxy, 1 from httpd1 & 2 from httpd2
    h1np5accepted3: CreateConnections(id: "h1np5accepted3", ipAddresses: ["192.168.0.1"], count: [3]) {
      id
    }

    # 3 connections connected from HA proxy weblogic port, 2 to weblogic wl1 & 1 to weblogic wl2
    h1np5connected8: CreateConnections(id: "h1np5connected8", ipAddresses: ["192.168.0.1"], count: [1]) {
      id
    }
    h1np5connected9: CreateConnections(id: "h1np5connected9", ipAddresses: ["192.168.0.1"], count: [1]) {
      id
    }
    h1np5connected10: CreateConnections(id: "h1np5connected10", ipAddresses: ["192.168.0.1"], count: [1]) {
      id
    }
    # 2 connections accepted by weblogic wl1 from HA proxy
    h1np3accepted: CreateConnections(id: "h1np3accepted", ipAddresses: ["10.0.0.1"], count: [2]) {
      id
    }

    # 1 connections accepted by weblogic wl2 from HA proxy
    h1np4accepted: CreateConnections(id: "h1np4accepted", ipAddresses: ["10.0.0.1"], count: [1]) {
      id
    }



    # Adding socket addresses to accepted connections
    h1np5accepted1csa: AddConnectionsSockAddress(from: {id: "h1np5accepted1"}, to: {id:"10.0.0.1:443"}) {
      from {
        id
      }
    }
    h1np5accepted2csa: AddConnectionsSockAddress(from: {id: "h1np5accepted2"}, to: {id:"10.0.0.1:443"}) {
      from {
        id
      }
    }
    h1np5accepted3csa: AddConnectionsSockAddress(from: {id: "h1np5accepted3"}, to: {id:"10.0.0.1:9001"}) {
      from {
        id
      }
    }

    h1np1acceptedcsa: AddConnectionsSockAddress(from: {id: "h1np1accepted"}, to: {id:"192.168.0.1:8080"}) {
      from {
        id
      }
    }
    h1np2acceptedcsa: AddConnectionsSockAddress(from: {id: "h1np2accepted"}, to: {id:"192.168.0.1:8090"}) {
      from {
        id
      }
    }
    h1np3acceptedcsa: AddConnectionsSockAddress(from: {id: "h1np3accepted"}, to: {id:"192.168.0.1:7001"}) {
      from {
        id
      }
    }
    h1np4acceptedcsa: AddConnectionsSockAddress(from: {id: "h1np4accepted"}, to: {id:"192.168.0.1:7002"}) {
      from {
        id
      }
    }


    # Adding socket addresses to connected connections
    # Adding socket addresses to 7 connected connections of HA proxy to httpd1 & httpd2
    h1np5connected1csa: AddConnectionsSockAddress(from: {id: "h1np5connected1"}, to: {id:"192.168.0.1:8080"}) {
      from {
        id
      }
    }
    h1np5connected2csa: AddConnectionsSockAddress(from: {id: "h1np5connected2"}, to: {id:"192.168.0.1:8080"}) {
      from {
        id
      }
    }
    h1np5connected3csa: AddConnectionsSockAddress(from: {id: "h1np5connected3"}, to: {id:"192.168.0.1:8080"}) {
      from {
        id
      }
    }
    h1np5connected4csa: AddConnectionsSockAddress(from: {id: "h1np5connected4"}, to: {id:"192.168.0.1:8080"}) {
      from {
        id
      }
    }
    h1np5connected5csa: AddConnectionsSockAddress(from: {id: "h1np5connected5"}, to: {id:"192.168.0.1:8090"}) {
      from {
        id
      }
    }
    h1np5connected6csa: AddConnectionsSockAddress(from: {id: "h1np5connected6"}, to: {id:"192.168.0.1:8090"}) {
      from {
        id
      }
    }
    h1np5connected7csa: AddConnectionsSockAddress(from: {id: "h1np5connected7"}, to: {id:"192.168.0.1:8090"}) {
      from {
        id
      }
    }
    # Adding socket addresses to 1 connected connections of httpd1 to HA proxy
    h1np1connected1csa: AddConnectionsSockAddress(from: {id: "h1np1connected1"}, to: {id:"10.0.0.1:9001"}) {
      from {
        id
      }
    }
    # Adding socket addresses to 2 connected connections of httpd2 to HA proxy
    h1np2connected1csa: AddConnectionsSockAddress(from: {id: "h1np2connected1"}, to: {id:"10.0.0.1:9001"}) {
      from {
        id
      }
    }
    h1np2connected2csa: AddConnectionsSockAddress(from: {id: "h1np2connected2"}, to: {id:"10.0.0.1:9001"}) {
      from {
        id
      }
    }
    # Adding socket addresses to 3 connected connections of HA proxy to weblogics, 2 to wl1 & 1 to wl2
    h1np5connected8csa: AddConnectionsSockAddress(from: {id: "h1np5connected8"}, to: {id:"192.168.0.1:7001"}) {
      from {
        id
      }
    }
    h1np5connected9csa: AddConnectionsSockAddress(from: {id: "h1np5connected9"}, to: {id:"192.168.0.1:7001"}) {
      from {
        id
      }
    }
    h1np5connected10csa: AddConnectionsSockAddress(from: {id: "h1np5connected10"}, to: {id:"192.168.0.1:7002"}) {
      from {
        id
      }
    }


    # Connect native process to their accepted connections
    npa1: AddNativeProcessAccepted(from: {id: "h1np1"}, to: {id: "h1np1accepted"}) {
      from {
        id
      }
    }
    npa2: AddNativeProcessAccepted(from: {id: "h1np2"}, to: {id: "h1np2accepted"}) {
      from {
        id
      }
    }
    npa3: AddNativeProcessAccepted(from: {id: "h1np3"}, to: {id: "h1np3accepted"}) {
      from {
        id
      }
    }
    npa4: AddNativeProcessAccepted(from: {id: "h1np4"}, to: {id: "h1np4accepted"}) {
      from {
        id
      }
    }
    npa5: AddNativeProcessAccepted(from: {id: "h1np5"}, to: {id: "h1np5accepted1"}) {
      from {
        id
      }
    }
    npa6: AddNativeProcessAccepted(from: {id: "h1np5"}, to: {id: "h1np5accepted2"}) {
      from {
        id
      }
    }
    npa7: AddNativeProcessAccepted(from: {id: "h1np5"}, to: {id: "h1np5accepted3"}) {
      from {
        id
      }
    }

    # Connect native process to their connected connections
    npc1: AddNativeProcessConnected(from: {id: "h1np5"}, to: {id: "h1np5connected1"}) {
      from {
        id
      }
    }
    npc2: AddNativeProcessConnected(from: {id: "h1np5"}, to: {id: "h1np5connected2"}) {
      from {
        id
      }
    }
    npc3: AddNativeProcessConnected(from: {id: "h1np5"}, to: {id: "h1np5connected3"}) {
      from {
        id
      }
    }
    npc4: AddNativeProcessConnected(from: {id: "h1np5"}, to: {id: "h1np5connected4"}) {
      from {
        id
      }
    }
    npc5: AddNativeProcessConnected(from: {id: "h1np5"}, to: {id: "h1np5connected5"}) {
      from {
        id
      }
    }
    npc6: AddNativeProcessConnected(from: {id: "h1np5"}, to: {id: "h1np5connected6"}) {
      from {
        id
      }
    }
    npc7: AddNativeProcessConnected(from: {id: "h1np5"}, to: {id: "h1np5connected7"}) {
      from {
        id
      }
    }
    npc8: AddNativeProcessConnected(from: {id: "h1np5"}, to: {id: "h1np5connected8"}) {
      from {
        id
      }
    }
    npc9: AddNativeProcessConnected(from: {id: "h1np5"}, to: {id: "h1np5connected9"}) {
      from {
        id
      }
    }
    npc10: AddNativeProcessConnected(from: {id: "h1np5"}, to: {id: "h1np5connected10"}) {
      from {
        id
      }
    }
    npc11: AddNativeProcessConnected(from: {id: "h1np1"}, to: {id: "h1np1connected1"}) {
      from {
        id
      }
    }
    npc12: AddNativeProcessConnected(from: {id: "h1np2"}, to: {id: "h1np2connected1"}) {
      from {
        id
      }
    }
    npc13: AddNativeProcessConnected(from: {id: "h1np2"}, to: {id: "h1np2connected2"}) {
      from {
        id
      }
    }

    # Add 5 native processes to host1
    ah1np1: AddHostNativeProcesses(from: {id: "host1"}, to: {id: "h1np1"}) {
      from {
        id
      }
    }
    ah1np2: AddHostNativeProcesses(from: {id: "host1"}, to: {id: "h1np2"}) {
      from {
        id
      }
    }
    ah1np3: AddHostNativeProcesses(from: {id: "host1"}, to: {id: "h1np3"}) {
      from {
        id
      }
    }
    ah1np4: AddHostNativeProcesses(from: {id: "host1"}, to: {id: "h1np4"}) {
      from {
        id
      }
    }
    ah1np5: AddHostNativeProcesses(from: {id: "host1"}, to: {id: "h1np5"}) {
      from {
        id
      }
    }


  }

`;
