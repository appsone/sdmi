package com.appnomic.sdmi.processor.impl;

import java.io.EOFException;
import java.io.Reader;
import java.io.Writer;
import java.util.Collections;

import com.appnomic.sdmi.transformer.RecordTransformer;
import com.appnomic.sdmi.transformer.impl.AssocRecordTransformer;

public class AssociationFileProcessor extends AbstractFileProcessor {

    private final String fromEntityName;
    private final String toEntityName;

    public AssociationFileProcessor(final String fromEntityName, final String toEntityName, final Reader inputReader,
            final Writer outputWriter) {
        super(fromEntityName + "_" + toEntityName, inputReader, outputWriter, true);
        this.fromEntityName = fromEntityName;
        this.toEntityName = toEntityName;
    }

    @Override
    protected RecordTransformer createInputRecordTransformer() throws EOFException {
        return new AssocRecordTransformer(fromEntityName, toEntityName,
                Collections.unmodifiableList(headerColummns));
    }

}
