package com.appnomic.sdmi.processor;

import java.io.IOException;

import com.appnomic.sdmi.exception.UnskippableIOException;

public interface FileProcessor {

    /**
     * @return false if the processing was partially successful for some of the
     *         input records of this processor, true if all input records of this
     *         processor were processed successfully.
     * @throws IOException if a non-record-continuable exception occurs while
     *                     reading this file
     */
    public boolean process() throws IOException, UnskippableIOException;

}
