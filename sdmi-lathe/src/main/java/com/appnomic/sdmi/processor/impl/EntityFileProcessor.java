package com.appnomic.sdmi.processor.impl;

import java.io.Reader;
import java.io.Writer;
import java.util.Collections;

import com.appnomic.sdmi.transformer.RecordTransformer;
import com.appnomic.sdmi.transformer.impl.EntityRecordTransformer;

public class EntityFileProcessor extends AbstractFileProcessor {

    private final String entityName;

    public EntityFileProcessor(final String entityName, final Reader inputReader, final Writer outputWriter) {
        super(entityName, inputReader, outputWriter, false);
        this.entityName = entityName;
    }

    @Override
    protected RecordTransformer createInputRecordTransformer() {
        return new EntityRecordTransformer(entityName,
                Collections.unmodifiableList(headerColummns));

    }

}
