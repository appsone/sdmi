package com.appnomic.sdmi.processor.impl;

import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appnomic.sdmi.exception.RecordTransformationException;
import com.appnomic.sdmi.exception.UnskippableIOException;
import com.appnomic.sdmi.parser.FileParser;
import com.appnomic.sdmi.parser.InputColumn;
import com.appnomic.sdmi.parser.InputRecord;
import com.appnomic.sdmi.parser.impl.CsvFileParser;
import com.appnomic.sdmi.parser.impl.CsvInputColumn;
import com.appnomic.sdmi.processor.FileProcessor;
import com.appnomic.sdmi.transformer.RecordTransformer;

public abstract class AbstractFileProcessor implements FileProcessor {

    private static final String QUALIFIERS_DELIMITING_REGEX = ":";
    private static final String PER_QUALIFIER_DELIMITING_REGEX = ";";

    private static final String TYPE_QUALIFIER_REGEX = "type\\s*=\\s*(.+)\\s*";
    private static final String SEPARATOR_QUALIFIER_REGEX = "separator\\s*=\\s*(.+)\\s*";

    private static final Pattern typeQualifierPattern = Pattern.compile(TYPE_QUALIFIER_REGEX, Pattern.CASE_INSENSITIVE);
    private static final Pattern separatorQualifierPattern = Pattern.compile(SEPARATOR_QUALIFIER_REGEX,
            Pattern.CASE_INSENSITIVE);

    private static final Logger logger = LoggerFactory.getLogger(AbstractFileProcessor.class);

    public static final String DEFAULT_COLUMN_TYPE = "STRING";

    private final String resourceName;
    private final Reader inputReader;
    private final Writer outputWriter;
    private final boolean ignoreListQualifier;

    private RecordTransformer inputRecordTransformer;

    protected final List<InputColumn> headerColummns;

    public AbstractFileProcessor(final String resourceName, final Reader inputReader, final Writer outputWriter,
            final boolean ignoreListQualifier) {
        this.resourceName = resourceName;
        this.inputReader = inputReader;
        this.outputWriter = outputWriter;
        this.ignoreListQualifier = ignoreListQualifier;
        headerColummns = new ArrayList<InputColumn>(8);
    }

    @Override
    public final boolean process() throws IOException, UnskippableIOException {
        logger.info("Parsing resource '{}'", resourceName);

        try (final FileParser csvParser = new CsvFileParser(inputReader)) {

            final Iterator<InputRecord> recordsItr = csvParser.iterator();

            // This check is actually useless,
            // since Commons CSV library has records size
            // as 1, even if the file is empty.
            if (null == recordsItr || !recordsItr.hasNext())
                throw new EOFException("Unable to parse really empty CSV file for resource '" + resourceName + "'");

            // Process the header row of the CSV file
            processHeaderInputRecord(recordsItr.next());

            if (0 == headerColummns.size())
                throw new EOFException("Unable to parse CSV file with no columns, for resource '" + resourceName + "'");

            // Now that the header record is processed,
            // create the appropriate input record transformer
            // (depending on what type of resource we're processing)
            // to process the remaining records
            inputRecordTransformer = createInputRecordTransformer();

            // Process the remaining records of the resource
            logger.info("Processing records of resource '{}'", resourceName);
            int numTotalRecords = 0;
            int numSuccessfulRecords = 0;
            boolean anyRecordFailed = false;
            while (recordsItr.hasNext()) {
                final InputRecord currRecord = recordsItr.next();
                String transformedRecord;

                try {
                    transformedRecord = inputRecordTransformer.transform(currRecord);
                } catch (RecordTransformationException e) {
                    anyRecordFailed = true;
                    logger.warn(
                            "Input record '{}' of resource '{}' could not be successfully transformed, skipping this record and continuing with the next one if any",
                            currRecord, resourceName);
                    continue;
                }

                logger.debug("Input record '{}' of resource '{}' was successfully transformed to '{}'", currRecord,
                        resourceName, transformedRecord);
                try {
                    outputWriter.write(transformedRecord);
                    ++numSuccessfulRecords;
                } catch (IOException e) {
                    logger.error("Cannot write to output file while processing resource '{}'", resourceName);
                    anyRecordFailed = true;
                    throw new UnskippableIOException(resourceName);
                }
                logger.debug("Wrote transformed record '{}' of resource '{}' to output", transformedRecord,
                        resourceName);

                ++numTotalRecords;
            }

            logger.info("Wrote '{}'/'{}' records of resource '{}' to output", numSuccessfulRecords, numTotalRecords,
                    resourceName);
            return !anyRecordFailed;
        }
    }

    protected void processHeaderInputRecord(final InputRecord headerInputRecord) throws EOFException {
        logger.info("Processing header of resource '{}': '{}'", resourceName, headerInputRecord);

        if (null == headerInputRecord || headerInputRecord.isEmpty())
            throw new EOFException("Unable to process resource '" + resourceName + "' because of empty header row");

        int colIndex = 0;
        for (final String colText : headerInputRecord) {
            if (colText.isEmpty())
                logger.warn(
                        "Found empty header column no. '{}' while processing header of resource '{}', this column will not be used in output mutation calls",
                        colIndex, resourceName);

            headerColummns.add(parseHeaderColumnText(colIndex, colText, ignoreListQualifier));
            ++colIndex;
        }

        logger.info("Done determining '{}'-column-size header of resource '{}': '{}'", headerInputRecord.size(),
                resourceName, headerColummns);
    }

    protected InputColumn parseHeaderColumnText(final int colIndex, final String headerColText,
            final boolean ignoreListQualifier) {
        // Tokenize the header column text to obtain the column name and
        // its qualifiers (data-type, per-list-value-separator, etc.)
        final String[] colTextSplits = headerColText.split(QUALIFIERS_DELIMITING_REGEX, 2);
        final String colName = colTextSplits[0].trim();

        // If the header column text doesn't contain any qualifiers (e.g. datatype &
        // per-list-value-separator info), use default STRING datatype &
        // no (null) separator per-list-value-separator i.e. an atom data
        // i.e. a single, non-list, primitive type value like an integer, string, etc.
        String colType = DEFAULT_COLUMN_TYPE;
        if (1 == colTextSplits.length)
            return new CsvInputColumn(colIndex, colName, colType, null);

        // Else if the header column text contains any qualifiers (e.g. datatype &
        // per-qualifier-separator info), use them as specified.
        final String colQualifiersStr = colTextSplits[1].trim();
        final String[] colQualifiers = colQualifiersStr.split(PER_QUALIFIER_DELIMITING_REGEX);
        String colSeparator = null;
        for (final String colQualifier : colQualifiers) {

            final Matcher typeQualifierMatcher = typeQualifierPattern.matcher(colQualifier.trim());

            // Is the qualifier is a type qualifier
            // register the current column's datatype
            if (typeQualifierMatcher.matches()) {
                colType = typeQualifierMatcher.group(1).toUpperCase();
                continue;
            }

            if (!ignoreListQualifier) {
                // Is the qualifier is a separator qualifier
                // register the current column's separator
                final Matcher separatorQualifierMatcher = separatorQualifierPattern.matcher(colQualifier.trim());
                if (separatorQualifierMatcher.matches())
                    colSeparator = separatorQualifierMatcher.group(1);
            }
        }

        return new CsvInputColumn(colIndex, colName, colType, colSeparator);
    }

    protected abstract RecordTransformer createInputRecordTransformer() throws EOFException;

}
