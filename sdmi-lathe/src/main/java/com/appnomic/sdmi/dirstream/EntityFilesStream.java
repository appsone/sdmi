package com.appnomic.sdmi.dirstream;

import java.io.IOException;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityFilesStream extends GeneralFilesStream {

    private static final String ENTITY_CSV_FILENAME_REGEX = "((@\\p{Upper}+\\p{Alnum}*_)?(\\p{Upper}+\\p{Alnum}*{1}))\\.csv";
    private static final Pattern entityCsvRegexFileNamePattern = Pattern.compile(ENTITY_CSV_FILENAME_REGEX);

    private static final Logger logger = LoggerFactory.getLogger(EntityFilesStream.class);

    public EntityFilesStream(final String inputDir) throws IOException {
        super(inputDir, EntityFilesStream::isEntityCsv);
    }

    private static boolean isEntityCsv(final Path filePath) {
        if (entityCsvRegexFileNamePattern.matcher(filePath.getFileName().toString()).matches())
            return true;

        return false;
    }

    public static String getEntityName(final String fileName) {
        final Matcher matcher = entityCsvRegexFileNamePattern.matcher(fileName);
        
        if (!matcher.matches() || 3 != matcher.groupCount()) {
            logger.warn("Cannot get entity name from file '{}' is not a valid entity CSV file name, this shouldn't have happened", fileName);
            return null;
        }
        
        return matcher.group(3);
        
    }
}
