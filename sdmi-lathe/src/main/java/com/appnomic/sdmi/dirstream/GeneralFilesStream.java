package com.appnomic.sdmi.dirstream;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class GeneralFilesStream implements DirectoryStream<Path> {

    private final Stream<Path> dirStream;
    
    public GeneralFilesStream(final String inputDir, final Predicate<Path> filterPredicate) throws IOException {
        Path inputDirPath = Paths.get(inputDir);

        if (!Files.exists(inputDirPath) || !Files.isReadable(inputDirPath) || !Files.isDirectory(inputDirPath))
            throw new IOException("Invalid input files directory '" + inputDir
                    + "', please ensure the directory exists and is readable");

        dirStream = Files.list(inputDirPath).filter(filterPredicate).sorted();
    }

    @Override
    public void close() throws IOException {
        dirStream.close();
    }

    @Override
    public Iterator<Path> iterator() {
        return dirStream.iterator();
    }

}
