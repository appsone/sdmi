package com.appnomic.sdmi.dirstream;

import java.io.IOException;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AssocFilesStream extends GeneralFilesStream {

    private static final String ASSOC_CSV_FILENAME_REGEX = "(\\p{Upper}+\\p{Alnum}*)_(\\p{Upper}+\\p{Alnum}*)\\.csv";
    private static final Pattern assocCsvRegexFileNamePattern = Pattern.compile(ASSOC_CSV_FILENAME_REGEX);

    private static final Logger logger = LoggerFactory.getLogger(AssocFilesStream.class);

    public AssocFilesStream(final String inputDir) throws IOException {
        super(inputDir, AssocFilesStream::isAssocCsv);
    }

    private static boolean isAssocCsv(final Path filePath) {
        if (assocCsvRegexFileNamePattern.matcher(filePath.getFileName().toString()).matches())
            return true;

        return false;
    }

    public static String getFromEntityName(final String fileName) {
        final Matcher matcher = assocCsvRegexFileNamePattern.matcher(fileName);
        
        if (!matcher.matches() || 2 != matcher.groupCount()) {
            logger.warn("Cannot get from-entity name from the file '{}', it is not a valid association CSV file name, this shouldn't have happened", fileName);
            return null;
        }
        
        return matcher.group(1);
    }

    public static String getToEntityName(String fileName) {
        final Matcher matcher = assocCsvRegexFileNamePattern.matcher(fileName);
        
        if (!matcher.matches() || 2 != matcher.groupCount()) {
            logger.warn("Cannot get to-entity name from the file '{}', it is not a valid association CSV file name, this shouldn't have happened", fileName);
            return null;
        }
        
        return matcher.group(2);
    }

}
