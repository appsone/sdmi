package com.appnomic.sdmi.traverser.impl;

import java.io.Reader;
import java.io.Writer;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appnomic.sdmi.dirstream.EntityFilesStream;
import com.appnomic.sdmi.processor.FileProcessor;
import com.appnomic.sdmi.processor.impl.EntityFileProcessor;

public class EntityFilesTraverser extends AbstractFilesTraverser {

    private static final Logger logger = LoggerFactory.getLogger(EntityFilesTraverser.class);

    public EntityFilesTraverser(final DirectoryStream<Path> inputDirStream, final Writer outputWriter) {
        super(inputDirStream, outputWriter);
    }

    @Override
    protected FileProcessor createFileProcessor(final Path inputFilePath, final Reader inputFileReader) {
        logger.debug("Creating entity file processor for input file '{}'", inputFilePath);
        
        final String entityName = EntityFilesStream.getEntityName(inputFilePath.getFileName().toString());

        if (null == entityName || entityName.isEmpty()) {
            logger.warn(
                    "Unable to detect entity name for input entity CSV file '{}'",
                    inputFilePath);
            return null;
        }

        return new EntityFileProcessor(entityName, inputFileReader, outputWriter) ;
    }

}
