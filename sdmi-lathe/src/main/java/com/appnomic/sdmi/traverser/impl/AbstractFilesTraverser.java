package com.appnomic.sdmi.traverser.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appnomic.sdmi.exception.UnskippableIOException;
import com.appnomic.sdmi.processor.FileProcessor;
import com.appnomic.sdmi.traverser.DirStreamTraverser;

public abstract class AbstractFilesTraverser implements DirStreamTraverser {

    private static final Logger logger = LoggerFactory.getLogger(AbstractFilesTraverser.class);

    private final DirectoryStream<Path> dirStream;
    protected final Writer outputWriter;

    public AbstractFilesTraverser(final DirectoryStream<Path> dirStream, final Writer outputWriter) {
        this.dirStream = dirStream;
        this.outputWriter = outputWriter;
    }

    @Override
    public final void traverse() throws UnskippableIOException {
        for (final Path inputFilePath : dirStream) {
            Reader inputFileReader = null;
            try {
                logger.info("Processing CSV file '{}'", inputFilePath);
                inputFileReader = new BufferedReader(new FileReader(inputFilePath.toFile()));
                final FileProcessor fileProcessor = createFileProcessor(inputFilePath, inputFileReader);
                if (null == fileProcessor) {
                    logger.warn(
                            "Unable to create file processor for input CSV file '{}', continuing with next CSV file if any",
                            inputFilePath);
                    continue;
                }

                if (!fileProcessor.process())
                    logger.warn("Input CSV file '{}' was processed, but only partially", inputFilePath);
                outputWriter.flush();

                logger.info("Done processing input CSV file '{}'", inputFilePath);
            } catch (IOException e) {
                logger.error("Error processing input CSV file '{}', continuing with next file if any", inputFilePath, e);
                continue;
            } catch (UnskippableIOException e) {
                logger.error("Encountered unskippable I/O exception when processing input CSV file '{}'", inputFilePath);
                throw e;
            }
            finally {
                if (null != inputFileReader)
                    try {
                        inputFileReader.close();
                    } catch (IOException e) {
                        logger.debug("Error closing file stream of input file '{}'", inputFilePath, e);
                    }
            }
        }        
    }

    protected abstract FileProcessor createFileProcessor(final Path inputFilePath, final Reader inputFileReader);

}
