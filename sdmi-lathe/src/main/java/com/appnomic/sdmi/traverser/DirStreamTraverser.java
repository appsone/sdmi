package com.appnomic.sdmi.traverser;

import com.appnomic.sdmi.exception.UnskippableIOException;

/**
 * The decision of what do to if an input file in this directory stream failed
 * to be processed - whether to continue with next file or give up completely on
 * all remaining input files - this decision is supposed to be taken by
 * implementations, typically in their
 * {@link com.appnomic.sdmi.traverser.DirStreamTraverser#traverse()} methods.
 *
 */
@FunctionalInterface
public interface DirStreamTraverser {

    public void traverse() throws UnskippableIOException;

}
