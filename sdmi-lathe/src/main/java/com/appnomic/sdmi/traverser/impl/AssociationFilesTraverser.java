package com.appnomic.sdmi.traverser.impl;

import java.io.Reader;
import java.io.Writer;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appnomic.sdmi.dirstream.AssocFilesStream;
import com.appnomic.sdmi.processor.FileProcessor;
import com.appnomic.sdmi.processor.impl.AssociationFileProcessor;

public class AssociationFilesTraverser extends AbstractFilesTraverser {

    private static final Logger logger = LoggerFactory.getLogger(AssociationFilesTraverser.class);

    public AssociationFilesTraverser(final DirectoryStream<Path> dirStream, final Writer outputWriter) {
        super(dirStream, outputWriter);
    }

    @Override
    protected FileProcessor createFileProcessor(final Path inputFilePath, final Reader inputFileReader) {
        logger.debug("Creating association file processor for input file '{}'", inputFilePath);
        
        final String fromEntityName = AssocFilesStream.getFromEntityName(inputFilePath.getFileName().toString());
        final String toEntityName = AssocFilesStream.getToEntityName(inputFilePath.getFileName().toString());
        
        if (null == fromEntityName || fromEntityName.isEmpty() || null == toEntityName || toEntityName.isEmpty()) {
            logger.warn(
                    "Unable to detect from/to entity names for input association CSV file '{}'",
                    inputFilePath);
            return null;
        }

        return new AssociationFileProcessor(fromEntityName, toEntityName, inputFileReader, outputWriter);
    }
}
