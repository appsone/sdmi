package com.appnomic.sdmi;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appnomic.sdmi.dirstream.AssocFilesStream;
import com.appnomic.sdmi.dirstream.EntityFilesStream;
import com.appnomic.sdmi.exception.UnskippableIOException;
import com.appnomic.sdmi.traverser.DirStreamTraverser;
import com.appnomic.sdmi.traverser.impl.AssociationFilesTraverser;
import com.appnomic.sdmi.traverser.impl.EntityFilesTraverser;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    @SuppressWarnings("unused")
    public static void main(String[] args) throws IOException {
        DirectoryStream<Path> entityFilesStream = null;
        DirectoryStream<Path> assocFileStream = null;
        Writer outputWriter = null;

        try {
            if (!validateArgs(args)) {
                printUsage();
                System.exit(-1);
            }

            logger.info("Processing input files in the input path '{}' to convert them to inventory graph file '{}'",
                    args[0], args[1]);

            // Processing entity CSV files
            logger.info("Processing entity files in the input path '{}'", args[0]);
            try {
                entityFilesStream = new EntityFilesStream(args[0]);
            } catch (IOException e) {
                logger.error(
                        "Cannot create entities files stream for input directory '{}', no point continuing further",
                        args[0], e);
                if (null != entityFilesStream)
                    entityFilesStream.close();
                System.exit(-60);
            }

            DirStreamTraverser entityStreamTraverser = null;
            try {
                outputWriter = new BufferedWriter(new FileWriter(args[1]));
            } catch (IOException e) {
                logger.error("Cannot write to output file '{}', no input file has been processed", args[1], e);
                if (null != entityFilesStream)
                    entityFilesStream.close();
                if (null != outputWriter)
                    outputWriter.close();
                System.exit(-61);
            }

            entityStreamTraverser = new EntityFilesTraverser(entityFilesStream, outputWriter);
            entityStreamTraverser.traverse();
            entityFilesStream.close();
            logger.info("Completed processing entity files in the input path '{}'", args[0]);

            // Processing association CSV files
            logger.info("Processing association files in the input path '{}'", args[0]);
            try {
                assocFileStream = new AssocFilesStream(args[0]);
            } catch (IOException e) {
                logger.error(
                        "Cannot create association files stream for input directory '{}', no point continuing further",
                        args[0], e);
                if (null != assocFileStream)
                    assocFileStream.close();
                System.exit(-62);
            }

            final DirStreamTraverser assocStreamTraverser = new AssociationFilesTraverser(assocFileStream, outputWriter);
            assocStreamTraverser.traverse();
            assocFileStream.close();
            logger.info("Completed processing association files in the input path '{}'", args[0]);

            outputWriter.close();
            logger.info(
                    "Completed processing all input files in the input path '{}' and converted them to inventory graph file '{}'",
                    args[0], args[1]);
        } catch (UnskippableIOException e) {
            logger.error("Encountered unskippable I/O exception", e);
            if (null != entityFilesStream)
                entityFilesStream.close();
            if (null != assocFileStream)
                assocFileStream.close();
            if (null != outputWriter)
                outputWriter.close();
            System.exit(-63);
        } catch (Throwable t) {
            logger.error("Unexpected error occurred", t);
            if (null != entityFilesStream)
                entityFilesStream.close();
            if (null != assocFileStream)
                assocFileStream.close();
            if (null != outputWriter)
                outputWriter.close();
            System.exit(-64);
        }
    }

    private static boolean validateArgs(final String[] args) {
        if (2 != args.length) {
            logger.error("Incorrect no. of input arguments '{}'", args.length);
            return false;
        }

        final String inputDir = args[0];
        final String outputFile = args[1];

        if (null == outputFile || outputFile.isEmpty()) {
            logger.error("Output file name cannot be empty");
            return false;
        }

        if (!Files.isDirectory(Paths.get(inputDir))) {
            logger.error("Input directory '{}' is not a valid directory, make sure it exists, is readable & contains input record files", inputDir);
            return false;
        }
        return true;
    }

    private static void printUsage() {
        System.out.println("           .___      .__          .__          __  .__             \r\n" + 
                "  ______ __| _/_____ |__|         |  | _____ _/  |_|  |__   ____   \r\n" + 
                " /  ___// __ |/     \\|  |  ______ |  | \\__  \\\\   __\\  |  \\_/ __ \\  \r\n" + 
                " \\___ \\/ /_/ |  Y Y  \\  | /_____/ |  |__/ __ \\|  | |   Y  \\  ___/  \r\n" + 
                "/____  >____ |__|_|  /__|         |____(____  /__| |___|  /\\___  > \r\n" + 
                "     \\/     \\/     \\/                       \\/          \\/     \\/  ");
        System.out.println(" __________________________________________________________________\r\n" + 
                "/_____/_____/_____/_____/_____/_____/_____/_____/_____/_____/_____/\r\n" + 
                "");
        System.out.println("USAGE");
        System.out.println("-----");
        System.out.println("$ java -jar sdmi-lathe.jar <inputDir> <outputFile>");
        System.out.println("      <inputDir>   - Directory inside which CSVs for inventory entities & associations");
        System.out.println("      <outputFile> - File to which GraphQL mutation output will be written");
    }
}
