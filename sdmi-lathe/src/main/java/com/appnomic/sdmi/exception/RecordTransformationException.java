package com.appnomic.sdmi.exception;

import com.appnomic.sdmi.parser.InputRecord;

/**
 * An exception indicating that there was an error while transforming an input
 * record. This is expected to be treated as a recoverable record-skippable
 * exception, i.e. the caller should log this exception and proceed with the
 * processing of the next input record if any.
 */
public class RecordTransformationException extends Exception {

    private static final long serialVersionUID = -6656341032949432599L;

    private final InputRecord inputRecord;

    public RecordTransformationException(final String message, final InputRecord inputRecord) {
        super(message);
        this.inputRecord = inputRecord;

    }
    
    public InputRecord getInputRecord() {
        return inputRecord;
    }

}
