package com.appnomic.sdmi.exception;

/**
 * An I/O exception that is not recoverable and the caller is expected to discontinue
 * all processing and end the program.
 * 
 * While reading input files, I/O exceptions
 * may occur but they can be treated as recoverable by skipping that particular input file
 * and continuing with the next. 
 * 
 * However, SDMI lathe treats certain kinds of I/O errors as
 * non-recoverable i.e. non-skippable exceptions. E.g. If while processing the input
 * files, an I/O exception occurs when writing intermediate results to the output file, 
 * there is typically no point conituing further. Such I/O exceptions are wrapped into this
 * unskippable I/O exception.
 */
public class UnskippableIOException extends Exception {

    private static final long serialVersionUID = 3469120881731868348L;

    private final String resourceEncounteredWhenThisOccured;
    
    public UnskippableIOException(final String resourceEncounteredWhenThisOccured) {
        this.resourceEncounteredWhenThisOccured = resourceEncounteredWhenThisOccured;
    }
    
    public String getResourceEncounteredWhenThisOccured() {
        return resourceEncounteredWhenThisOccured;
    }
}
