package com.appnomic.sdmi.transformer.impl;

import java.io.EOFException;
import java.util.List;

import com.appnomic.sdmi.exception.RecordTransformationException;
import com.appnomic.sdmi.parser.InputColumn;
import com.appnomic.sdmi.parser.InputRecord;

public class AssocRecordTransformer extends AbstractRecordTransformer {

    private static final String MUTATION_PREFIX_VERB = "Add";

    private final String fromEntityName;
    private final String toEntityName;
    private final String assocName;
    private final String mutationPrefix;

    public AssocRecordTransformer(final String fromEntityName, final String toEntityName, final List<InputColumn> columns) throws EOFException {
        super(columns);
        this.fromEntityName = fromEntityName;
        this.toEntityName = toEntityName;
        assocName = fromEntityName + "_" + toEntityName;
        mutationPrefix = MUTATION_PREFIX_VERB + this.fromEntityName + this.toEntityName + "(";
        
        int numColsWithNonEmptyNames = 0;
        for (final InputColumn column: columns) {
            if (null != column && null != column.getName() && !column.getName().isEmpty())
                ++numColsWithNonEmptyNames;
        }
        
        if (2 > numColsWithNonEmptyNames)
            throw new EOFException("Input for association '" + assocName + "' contains '" + numColsWithNonEmptyNames
                    + "' non-empty columns, need at least 2 such columns");
    }
    
    @Override
    public String transform(final InputRecord inputRecord) throws RecordTransformationException {
        // Cry about row-column size mismatch if any
        // but allow row size < no. of columns
        if (numColumns < inputRecord.size())
            throw new RecordTransformationException(
                    "Row size '" + inputRecord.size() + "' greater than no. of columns '" + numColumns + "'",
                    inputRecord);

        String fromId = "";
        final StringBuilder ret = new StringBuilder(mutationPrefix);
        int numColsProcessed = 0;
        for (int i = 0; i < inputRecord.size(); i++) {
            // Process an input record field only if its corresponding column is non-empty
            if (null != columns.get(i).getName() && !columns.get(i).getName().isEmpty()) {
                
                if (0 == numColsProcessed) {
                    fromId = columns.get(i).getName();
                    ret.append("from: {" + fromId + ": ");
                    
                    final String fieldEnclosingStr = fieldEnclosingStrings[i];
                    ret.append(fieldEnclosingStr).append(inputRecord.get(i).replace("\"", "\\\""))
                            .append(fieldEnclosingStr);
                    
                    ret.append("}, ");
                }
                else if (1 == numColsProcessed) {
                    ret.append("to: {" + columns.get(i).getName() + ": ");

                    final String fieldEnclosingStr = fieldEnclosingStrings[i];
                    ret.append(fieldEnclosingStr).append(inputRecord.get(i).replace("\"", "\\\""))
                            .append(fieldEnclosingStr);

                    ret.append("}");
                }

                ++numColsProcessed;
                // Process only from-id and to-id 
                // (the first two non-empty) columns in an association
                if (2 == numColsProcessed)
                    break;
            }
        }
        
        ret.append(") {" + LINE_TERMINATOR);
        
        if (2 == numColsProcessed) {
            ret.append("    from {" + LINE_TERMINATOR);
            ret.append("        " + fromId + LINE_TERMINATOR);
            ret.append("    }" + LINE_TERMINATOR);
        }
        
        ret.append("}" + LINE_TERMINATOR);
        
        return ret.toString();
    }

}
