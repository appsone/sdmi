package com.appnomic.sdmi.transformer.impl;

import java.util.List;

import com.appnomic.sdmi.parser.InputColumn;
import com.appnomic.sdmi.processor.impl.EntityFileProcessor;
import com.appnomic.sdmi.transformer.RecordTransformer;

public abstract class AbstractRecordTransformer implements RecordTransformer {

    protected final List<InputColumn> columns;
    protected final int numColumns;
    protected final String[] fieldEnclosingStrings;

    public AbstractRecordTransformer(final List<InputColumn> columns) {
        this.columns = columns;
        numColumns = columns.size();

        fieldEnclosingStrings = new String[numColumns];
        for (int i = 0; i < numColumns; i++) {
            fieldEnclosingStrings[i] = (this.columns.get(i).getType() == EntityFileProcessor.DEFAULT_COLUMN_TYPE)
                    ? DEFAULT_FIELD_ENCLOSING_CHAR
                    : "";
        }
    }
}
