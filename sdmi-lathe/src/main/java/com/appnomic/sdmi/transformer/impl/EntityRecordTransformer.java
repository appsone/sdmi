package com.appnomic.sdmi.transformer.impl;

import java.util.List;
import java.util.regex.Pattern;

import com.appnomic.sdmi.exception.RecordTransformationException;
import com.appnomic.sdmi.parser.InputColumn;
import com.appnomic.sdmi.parser.InputRecord;

public class EntityRecordTransformer extends AbstractRecordTransformer {

    private static final String MUTATION_PREFIX_VERB = "Create";

    private final String entityName;
    private final String mutationPrefix;

    public EntityRecordTransformer(final String entityName, final List<InputColumn> columns) {
        super(columns);
        this.entityName = entityName;
        mutationPrefix = MUTATION_PREFIX_VERB + this.entityName + "(";
    }

    @Override
    public String transform(final InputRecord inputRecord) throws RecordTransformationException {
        // Cry about row-column size mismatch if any
        // but allow row size < no. of columns
        if (numColumns < inputRecord.size())
            throw new RecordTransformationException(
                    "Row size '" + inputRecord.size() + "' greater than no. of columns '" + numColumns + "'",
                    inputRecord);

        final StringBuilder ret = new StringBuilder(mutationPrefix);
        for (int i = 0; i < inputRecord.size(); i++) {
            // Process an input record field only if its corresponding column is non-empty
            if (null != columns.get(i).getName() && !columns.get(i).getName().isEmpty()) {
                ret.append(columns.get(i).getName()).append(": ");

                final String fieldSeparator = columns.get(i).getSeparator();
                final String fieldEnclosingStr = fieldEnclosingStrings[i];

                if (null == fieldSeparator)
                    ret.append(fieldEnclosingStr).append(inputRecord.get(i).replace("\"", "\\\"")).append(fieldEnclosingStr);
                else
                    ret.append(delimitListField(inputRecord.get(i).replace("\"", "\\\""), fieldEnclosingStr, fieldSeparator));

                ret.append(", ");
            }
        }

        // Remove the extra trailing 2-char string ", "
        if (1 <= columns.size())
            ret.setLength(ret.length() - 2);

        // Close the mutation call
        ret.append(") {" + LINE_TERMINATOR);
        if (0 < columns.size())
            ret.append("    ").append(columns.get(0).getName());
        ret.append(LINE_TERMINATOR + "}" + LINE_TERMINATOR);

        return ret.toString();
    }

    private static String delimitListField(final String listField, final String fieldEnclosingStr,
            final String separator) {
        final StringBuilder ret = new StringBuilder();
        final String[] splitListFields = listField.split(Pattern.quote(separator));

        ret.append("[");

        for (final String splitListField : splitListFields) {
            ret.append(fieldEnclosingStr).append(splitListField).append(fieldEnclosingStr).append(", ");
        }
        if (2 <= ret.length())
            ret.setLength(ret.length() - 2);

        ret.append("]");

        return ret.toString();
    }
}
