package com.appnomic.sdmi.transformer;

import com.appnomic.sdmi.exception.RecordTransformationException;
import com.appnomic.sdmi.parser.InputRecord;

@FunctionalInterface
public interface RecordTransformer {
    
    public static final String DEFAULT_FIELD_ENCLOSING_CHAR = "\"";
    public static final String LINE_TERMINATOR = "\n";

    public String transform(final InputRecord inputRecord) throws RecordTransformationException;
    
}
