package com.appnomic.sdmi.parser.impl;

import com.appnomic.sdmi.parser.InputColumn;

public class CsvInputColumn implements InputColumn {

    private final int index;
    private final String name;
    private final String type;
    private final String separator;

    public CsvInputColumn(final int index, final String name, final String type, final String separator) {
        this.index = index;
        this.name = name;
        this.type = type;
        this.separator = separator;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getSeparator() {
        return separator;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + index;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((separator == null) ? 0 : separator.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CsvInputColumn other = (CsvInputColumn) obj;
        if (index != other.index)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (separator == null) {
            if (other.separator != null)
                return false;
        } else if (!separator.equals(other.separator))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "CsvHeaderColumnImpl [index=" + index + ", name=" + name + ", type=" + type + ", separator=" + separator
                + "]";
    }

}
