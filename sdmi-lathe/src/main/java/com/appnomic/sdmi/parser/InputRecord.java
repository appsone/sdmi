package com.appnomic.sdmi.parser;

public interface InputRecord extends Iterable<String> {
    
    public int size();
    
    public boolean isEmpty();
    
    public String get(final int index);

}
