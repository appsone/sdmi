package com.appnomic.sdmi.parser.impl;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.appnomic.sdmi.parser.FileParser;
import com.appnomic.sdmi.parser.InputRecord;

public class CsvFileParser implements FileParser {

    private static final char CSV_CHAR_DELIMITER = ',';
    private final CSVFormat csvFormat = CSVFormat.EXCEL.withIgnoreEmptyLines(false).withTrim()
            .withDelimiter(CSV_CHAR_DELIMITER);

    private final CSVParser delegate;
    private final CsvRowIterator csvRowIterator;

    public CsvFileParser(final Reader reader) throws IOException {
        delegate = new CSVParser(reader, csvFormat);
        csvRowIterator = new CsvRowIterator();
    }

    @Override
    public Iterator<InputRecord> iterator() {
        return csvRowIterator;
    }

    @Override
    public void close() throws IOException {
        delegate.close();
    }

    private class CsvRowIterator implements Iterator<InputRecord> {

        @Override
        public boolean hasNext() {
            return delegate.iterator().hasNext();
        }

        @Override
        public InputRecord next() {
            final CSVRecord csvRecord = delegate.iterator().next();
            return new CsvInputRecord(csvRecord);
        }

    }
}
