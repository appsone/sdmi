package com.appnomic.sdmi.parser;

public interface InputColumn {

    public int getIndex();

    public String getName();

    public String getType();

    public String getSeparator();
}
