package com.appnomic.sdmi.parser.impl;

import java.util.Iterator;

import org.apache.commons.csv.CSVRecord;

import com.appnomic.sdmi.parser.InputRecord;

public class CsvInputRecord implements InputRecord {

    private final CSVRecord delegate;

    public CsvInputRecord(CSVRecord delegate) {
        this.delegate = delegate;
    }

    @Override
    public Iterator<String> iterator() {
        return delegate.iterator();
    }

    @Override
    public int size() {
        return delegate.size();
    }
    
    @Override
    public boolean isEmpty() {
        // Commons CSV library reports no. of columns
        // as 1, even if the row is empty.
        return 1 >= delegate.size();
    }

    @Override
    public String get(int index) {
        return delegate.get(index);
    }
    
    @Override
    public String toString() {
        return "CsvRowImpl [delegate=" + delegate + "]";
    }
    
    
}
