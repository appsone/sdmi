package com.appnomic.sdmi.parser;

import java.io.IOException;

public interface FileParser extends Iterable<InputRecord>, AutoCloseable {
    
    @Override
    public void close() throws IOException;
    

}
